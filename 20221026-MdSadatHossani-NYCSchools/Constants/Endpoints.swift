//
//  Endponts.swift
//  20221026-MdSadatHossani-NYCSchools
//
//  Created by Sadat on 10/26/22.
//

import Foundation

struct Endpoints {
    static let schoolsEndpoint = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let schooldetailsEndpoint = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="
}

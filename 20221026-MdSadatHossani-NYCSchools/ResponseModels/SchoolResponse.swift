//
//  School.swift
//  20221026-MdSadatHossani-NYCSchools
//
//  Created by Sadat on 10/26/22.
//

import Foundation

struct SchoolResponse: Codable {
    let dbn, schoolName, primaryAddressLine1, city: String?
    let zip, stateCode: String?
    let overveiwParagraph : String?
    
      enum CodingKeys: String, CodingKey {
          case dbn
          case schoolName = "school_name"
          case primaryAddressLine1 = "primary_address_line_1"
          case city, zip
          case stateCode = "state_code"
          case overveiwParagraph = "overview_paragraph"
      }
}

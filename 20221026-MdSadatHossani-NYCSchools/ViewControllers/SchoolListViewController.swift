//
//  ViewController.swift
//  20221026-MdSadatHossani-NYCSchools
//
//  Created by Sadat on 10/26/22.
//

import UIKit

class SchoolListViewController: UIViewController {
    
    // MARK: Propterties
    var networkManager : NetworkManagerProtocol = NetworkManager()
    var schoolResponse: [SchoolResponse] = []
    
    let schoolsTableView: UITableView = {
        let tableView = UITableView()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getSchoolResponse()
        setupTableView()
    }
    
    // MARK: Get Schools func
    
    func getSchoolResponse() {
        
            // get the data from the server
            networkManager.request([SchoolResponse].self, from: Endpoints.schoolsEndpoint) { [weak self] result in
                switch result {
                    
                    // in success case, assign schools data to schools property
                    // reload table view
                case .success(let response):
                    self?.schoolResponse = response
                    DispatchQueue.main.async {
                        self?.schoolsTableView.reloadData()
                    }
                    
                    // failure case
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
    }
    
    // MARK: SetupTableView
    func setupTableView() {
        //setup constraints for tableview
        view.addSubview(schoolsTableView)
        schoolsTableView.translatesAutoresizingMaskIntoConstraints = false
        schoolsTableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        schoolsTableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        schoolsTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        schoolsTableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        schoolsTableView.register(SchoolsTableViewCell.self, forCellReuseIdentifier: "schoolCell")
        schoolsTableView.backgroundColor = UIColor(named: "green")
        schoolsTableView.rowHeight = 200
        schoolsTableView.dataSource = self
        schoolsTableView.delegate = self
}

}

// MARK: SchoolListViewController Extension
extension SchoolListViewController: UITableViewDataSource, UITableViewDelegate {
    //MARK: - TableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //number of rows from response
        let rowCount = schoolResponse.count
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //deque reusable cell

        guard let cell = self.schoolsTableView.dequeueReusableCell(
            withIdentifier: "schoolCell",
            for: indexPath) as? SchoolsTableViewCell
        else { return UITableViewCell() }
        
        //setupvalues using schoolresponse model
        cell.setupValues(school: schoolResponse[indexPath.row])
        return cell
    }
    
    
    //MARK: - TableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dbn = schoolResponse[indexPath.row].dbn
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        guard let nextViewController = storyBoard.instantiateViewController(
            withIdentifier: "SchoolDetailsViewController") as? SchoolDetailsViewController
        else { return }
        
        nextViewController.dbn = dbn
        nextViewController.schoolOverView = schoolResponse[indexPath.row].overveiwParagraph
        nextViewController.schoolName = schoolResponse[indexPath.row].schoolName
        schoolsTableView.deselectRow(at: indexPath, animated: true)
        self.present(nextViewController, animated:true, completion:nil)
    }
}

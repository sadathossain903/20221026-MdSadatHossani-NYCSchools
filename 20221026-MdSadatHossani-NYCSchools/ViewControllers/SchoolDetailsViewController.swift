//
//  SchoolDetailsViewController.swift
//  20221026-MdSadatHossani-NYCSchools
//
//  Created by Sadat on 10/27/22.
//

import UIKit

class SchoolDetailsViewController: UIViewController {
    
    // MARK: Properties
    var dbn: String?
    var schoolDetails: SchoolDetailResponse?
    var schoolName: String?
    var schoolOverView: String?
    var networkManager : NetworkManagerProtocol = NetworkManager()
    
    // MARK: IBOUTLETS
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var schoolOverviewLabel: UILabel!
    @IBOutlet weak var readingScoreLabel: UILabel!
    @IBOutlet weak var mathScoreLabel: UILabel!
    @IBOutlet weak var writingScoreLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getSchoolDetails()
        schoolOverviewLabel.text = schoolOverView ?? "Data not available"
        schoolNameLabel.text = schoolName ?? "Data not available"
    }
    
    // MARK: Get School Details Func
    func getSchoolDetails() {
        
        // get the data from the server
        networkManager.request([SchoolDetailResponse].self, from: Endpoints.schooldetailsEndpoint + (dbn ?? "")) { [weak self] result in
            switch result {
                // in success case, assign schools data to schools details property
                // setup view
            case .success(let response):
                self?.schoolDetails = response.first
                DispatchQueue.main.async {
                    //in prod have some kind of shimmer view to show loading
                    self?.setupView()
                }
                // failure case
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    // MARK: SetupView func
    func setupView() {
        mathScoreLabel.text = schoolDetails?.satMathAvgScore ?? "Data not available"
        readingScoreLabel.text = schoolDetails?.satCriticalReadingAvgScore ?? "Data not available"
        writingScoreLabel.text = schoolDetails?.satWritingAvgScore ?? "Data not available"
        
    }
}

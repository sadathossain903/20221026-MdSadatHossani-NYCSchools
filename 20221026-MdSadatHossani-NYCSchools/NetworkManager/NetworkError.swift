//
//  NetworkError.swift
//  20221026-MdSadatHossani-NYCSchools
//
//  Created by Sadat on 10/27/22.
//

import Foundation

enum NetworkError: Error, LocalizedError {
    case badURL
    case other(Error)
    var errorDescription: String? {
        switch self {
        case .badURL:
            return "Bad url"
        case .other(let error):
            return error.localizedDescription
        }
    }
}

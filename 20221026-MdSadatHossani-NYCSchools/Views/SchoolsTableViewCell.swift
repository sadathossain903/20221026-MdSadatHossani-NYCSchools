//
//  SchoolsTableViewCell.swift
//  20221026-MdSadatHossani-NYCSchools
//
//  Created by Sadat on 10/27/22.
//

import UIKit

class SchoolsTableViewCell: UITableViewCell {
    
    
    let schoolName: UILabel = {
        let label = UILabel()
        label.text = "Data Not Available"
        label.textColor = UIColor(named: "brown")
        label.font = .systemFont(ofSize: 17, weight: .heavy)
        label.numberOfLines = 0
        return label
    }()
    
    let schoolAddress: UILabel = {
        let label = UILabel()
        label.text = "Data Not Available"
        label.textColor = UIColor(named: "brown")
        label.numberOfLines = 0
        return label
    }()
    
    // vertical stack view to arrange labels
    let stackView : UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.spacing = 1
        stack.distribution = .fillEqually
                
        return stack
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = UIColor(named: "cream")
        setupSchoolView()
        layout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func layout() {
        // set constraints
        NSLayoutConstraint.activate([
            // stack view constraints
            stackView.topAnchor.constraint(equalTo: topAnchor, constant: 30),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -30)
        ])
        
    }
  
    func setupValues(school: SchoolResponse) {
        
        schoolName.text = school.schoolName
        guard let primaryAdress = school.primaryAddressLine1,
              let stateCode = school.stateCode,
              let zip = school.zip
        else { return }
        schoolAddress.text =  "\(primaryAdress), \(stateCode), \(zip)"
    }
    
    func setupSchoolView() {
        addSubview(stackView)
        stackView.addArrangedSubview(schoolName)
        stackView.addArrangedSubview(schoolAddress)
    }
}
